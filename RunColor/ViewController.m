//
//  ViewController.m
//  RunColor
//
//  Created by Pavel Wasilenko on 2017-09-08.
//  Copyright © 2017 Alex Nordi. All rights reserved.
//

#import <Appodeal/Appodeal.h>

#import "ViewController.h"

#import "GameKitHelper.h"

static NSString * const adCountKey = @"adCountKey";
static NSString * const highScoreKey = @"highScoreKey";

@import WebKit;

@interface ViewController () <UIWebViewDelegate>

@property (strong, nonatomic) UIWebView *uiWebView;

@property (strong, nonatomic) UIButton *button;

@property (assign, nonatomic) NSInteger adCount;
@property (assign, nonatomic) NSInteger highScore;

@end


@implementation ViewController

- (void)addTestButton;
{
    _button = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 44.0f, 44.0f)];
    
    _button.backgroundColor = [UIColor redColor];
    
    [_button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_button];
    
    [self.view bringSubviewToFront:_button];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // read adCount / highScore from UserDef

    self.adCount = [[NSUserDefaults standardUserDefaults] integerForKey:adCountKey];
    self.highScore = [[NSUserDefaults standardUserDefaults] integerForKey:highScoreKey];

    NSString *gamePath = [NSBundle.mainBundle pathForResource:@"index" ofType:@"html" inDirectory:@"RunColorGame"];
    
    // This is the URL to be loaded into the WKWebView.
    NSURL *gameUrl = [NSURL fileURLWithPath:gamePath];

    
    //РАБОТАЕТ
    _uiWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    
    
    [self.view addSubview:_uiWebView];
    
    NSURLRequest * req = [NSURLRequest requestWithURL:gameUrl];
    
    _uiWebView.delegate = self;
    
    [_uiWebView loadRequest:req];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Nothing else happens here
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showAuthenticationViewController)
                                                 name:PresentAuthenticationViewControllerNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showLeaderboardViewController)
                                                 name:PresentLeaderboardViewControllerNotification
                                               object:nil];
    
    [[GameKitHelper sharedGameKitHelper]
     authenticateLocalPlayer];
}

#pragma mark - button

- (IBAction)buttonPressed:(id)sender {
    ;
}


#pragma mark - AppodealRewardedVideoDelegate

//- (void)rewardedVideoWillDismiss {
//    NSLog(@"видео реклама была закрыта");
//}
//    
//- (void)rewardedVideoDidFinish:(NSUInteger)rewardAmount name:(NSString *)rewardName;
//{
//    NSLog(@"видео было успешно просмотрено и пользователя можно наградить: %@ %@", @(rewardAmount), rewardName);
//}

//TODO: callback for AppoDeal


#pragma mark - WebView

- (BOOL)webView:(UIWebView*)webView
shouldStartLoadWithRequest:(NSURLRequest*)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"request = %@", request);
    
//    "runColorHighScore://"&be
    if ([[[request URL] scheme] isEqualToString:@"runcolorhighscore"]) {
        [webView stopLoading];
        NSLog(@"highscore arrived");
        
        NSNumber *highscore = [self numberFromUrl:request.URL];
        //TODO: GameKit
        
        [[GameKitHelper sharedGameKitHelper] setHighScore:(int64_t)highscore.longLongValue];
        
        return NO;
    } else if ([request.URL.scheme isEqualToString:@"runcolorshowad"]) {
        [webView stopLoading];
        NSLog(@"ad arrived");
        
//        NSNumber *adCount = [self numberFromUrl:request.URL];
        //TODO: ad start
        
        _adCount = _adCount + 1;
        
        if (_adCount > 2) {
            _adCount = 0;
            
            //TODO: showAD
            [Appodeal showAd:AppodealShowStyleNonSkippableVideo rootViewController:self];
        }
        
        [[NSUserDefaults standardUserDefaults] setInteger:_adCount forKey:adCountKey];
        
        return NO;
    } else if ([request.URL.scheme isEqualToString:@"runcolorshowhighscore"]) {
        //"runcolorshowhighscore://hi"
        
        [[GameKitHelper sharedGameKitHelper] showLeaderboard];
        
        return NO;
    }
//    if ([[[request URL] scheme] isEqualToString:@"twitter"]) {
//        [webView stopLoading];
//        
//        if ([[UIApplication sharedApplication] canOpenURL:request.URL]) {
//            [[UIApplication sharedApplication] openURL:[request URL]];
//        } else {
//            //TODO: built-in sharing
//            
//        }
//        
//        NSLog(@"twitter sharing");
//        return NO;
//    }
    else {
        return YES;
    }
}

- (NSNumber *)numberFromUrl:(NSURL *)url;
{
    NSArray *arr = [url.absoluteString componentsSeparatedByString:@"://"];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString: [arr lastObject]];
    
    return myNumber;
}

- (void)showAuthenticationViewController;
{
    GameKitHelper *gameKitHelper = [GameKitHelper sharedGameKitHelper];
    
    
    [self presentViewController:gameKitHelper.authenticationViewController
                       animated:YES
                     completion:nil];
}

- (void)showLeaderboardViewController;
{
    GameKitHelper *gameKitHelper = [GameKitHelper sharedGameKitHelper];
    
    [self presentViewController:gameKitHelper.leaderBoardViewController
                       animated:YES
                     completion:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
