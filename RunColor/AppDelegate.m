//
//  AppDelegate.m
//  RunColor
//
//  Created by Pavel Wasilenko on 2017-09-08.
//  Copyright © 2017 Alex Nordi. All rights reserved.
//

#import "AppDelegate.h"

#import <Appodeal/Appodeal.h>

#import <YandexMobileMetrica/YandexMobileMetrica.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [Appodeal initializeWithApiKey:@"94dce098cee8f0278c90bef0a9ea06ffe5a4a0128d263538" types:AppodealAdTypeNonSkippableVideo];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - YandexMobileMetrica
    
+ (void)initialize
    {
        if ([self class] == [AppDelegate class]) {
            /* Replace API_KEY with your unique API key. Please, read official documentation how to obtain one:
             https://tech.yandex.com/metrica-mobile-sdk/doc/mobile-sdk-dg/tasks/ios-quickstart-docpage/
             */
            [YMMYandexMetrica activateWithApiKey:@"d16ea83b-31c8-4026-8f02-defb7a2eb583"];
            //manual log setting for whole library
            [YMMYandexMetrica setLoggingEnabled:YES];
        }
    }

@end
