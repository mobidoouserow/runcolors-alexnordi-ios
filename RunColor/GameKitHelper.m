//
//  GameKitHelper.m
//  RunColor
//
//  Created by Pavel Wasilenko on 17-09-19.
//  Copyright © 2017 Alex Nordi. All rights reserved.
//


#import "GameKitHelper.h"

@import GameKit;

NSString *const PresentAuthenticationViewControllerNotification = @"present_authentication_view_controller";
NSString *const PresentLeaderboardViewControllerNotification = @"present_highscore_view_controller";

NSString *const kLeaderboardID = @"com.alexnordi.RunColor.HighscoreLeaderboard";

@interface GameKitHelper () <GKGameCenterControllerDelegate> {
    BOOL _enableGameCenter;
}

@property (nonatomic, copy, nullable) void (^setScoreBlock)();

@end

@implementation GameKitHelper

#pragma mark - lifecycle

- (id)init
{
    self = [super init];
    if (self) {
        _enableGameCenter = YES;
    }
    return self;
}

+ (instancetype)sharedGameKitHelper
{
    static GameKitHelper *sharedGameKitHelper;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedGameKitHelper = [[GameKitHelper alloc] init];
    });
    return sharedGameKitHelper;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setHighScore:(int64_t)highscore;
{
    __weak typeof(self) weakSelf = self;
    _setScoreBlock = ^void() {
        __strong typeof(self) strongSelf = weakSelf;
        
        if (strongSelf) {
            GKScore *score = [[GKScore alloc] initWithLeaderboardIdentifier:kLeaderboardID];
            score.value = highscore;
            
            [GKScore reportScores:@[score] withCompletionHandler:^(NSError * _Nullable error) {
                [strongSelf setLastError:error];
            }];
            
            strongSelf.setScoreBlock = nil;
        }
    };
    
    if (!_enableGameCenter) {
        [self authenticateLocalPlayer];
    } else {
        _setScoreBlock();
    }
}

- (void)authenticateLocalPlayer
{
    //1
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    //2
    
    __weak typeof(self) weakSelf = self;
    
    localPlayer.authenticateHandler  =
    ^(UIViewController *viewController, NSError *error) {
        __strong typeof(self) strongSelf = weakSelf;
        
        if (strongSelf) {
            //3
            [strongSelf setLastError:error];
            
            if(viewController != nil) {
                //4
                [self setAuthenticationViewController:viewController];
            } else if([GKLocalPlayer localPlayer].isAuthenticated) {
                //5
                _enableGameCenter = YES;
                if (strongSelf.setScoreBlock) {
                    strongSelf.setScoreBlock();
                }
            } else {
                //6
                _enableGameCenter = NO;
            }
        }
    };
}

- (void)setAuthenticationViewController:(UIViewController *)authenticationViewController
{
    if (authenticationViewController != nil) {
        _authenticationViewController = authenticationViewController;
        [[NSNotificationCenter defaultCenter]
         postNotificationName:PresentAuthenticationViewControllerNotification
         object:self];
    }
}


- (void)setLastError:(NSError *)error
{
    _lastError = [error copy];
    
    if (_lastError) {
        NSLog(@"GameKitHelper ERROR: %@", _lastError);
        NSLog(@"GameKitHelper ERROR DESCRIPTION: %@",
              [[_lastError userInfo] description]);
    }
}


-(void)showLeaderboard;
{
    GKGameCenterViewController *leaderboardController = [[GKGameCenterViewController alloc] init];
    
    if (leaderboardController != NULL)
    {
        leaderboardController.leaderboardIdentifier = kLeaderboardID;
        leaderboardController.viewState = GKGameCenterViewControllerStateLeaderboards;
        
        //TODO: transit to
        leaderboardController.gameCenterDelegate = self;
        
        [self setLeaderBoardViewController:leaderboardController];
//        UIViewController *vc = self.view.window.rootViewController;
//        [vc presentViewController: leaderboardController animated: YES completion:nil];
    }
}

- (void)setLeaderBoardViewController:(UIViewController *)leaderBoardViewController
{
    if (leaderBoardViewController != nil)
    {
        _leaderBoardViewController = leaderBoardViewController;
        [[NSNotificationCenter defaultCenter]
         postNotificationName:PresentLeaderboardViewControllerNotification
         object:self];
    }
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)viewController
{
//    UIViewController *vc = self.view.window.rootViewController;
//    [vc dismissViewControllerAnimated:YES completion:nil];
    
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

@end
