//
//  GameKitHelper.h
//  RunColor
//
//  Created by Pavel Wasilenko on 17-09-19.
//  Copyright © 2017 Alex Nordi. All rights reserved.
//

@import Foundation;
@import GameKit;

extern NSString *const PresentAuthenticationViewControllerNotification;
extern NSString *const PresentLeaderboardViewControllerNotification;

@interface GameKitHelper : NSObject

@property (nonatomic, readonly) UIViewController *authenticationViewController;
@property (nonatomic, readonly) UIViewController *leaderBoardViewController;
@property (nonatomic, readonly) NSError *lastError;

+ (instancetype)sharedGameKitHelper;

- (void)authenticateLocalPlayer;

- (void)setHighScore:(int64_t)highscore;

- (void)showLeaderboard;

@end
