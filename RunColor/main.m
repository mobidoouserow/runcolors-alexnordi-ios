//
//  main.m
//  RunColor
//
//  Created by Pavel Wasilenko on 2017-09-08.
//  Copyright © 2017 Alex Nordi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
